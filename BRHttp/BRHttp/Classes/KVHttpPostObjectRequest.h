//
//  KVHttpPostRequest.h
//  Rewardisement
//
//  Created by ELie Melki on 11/29/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVAbstractHttpRequest.h"

@protocol KVHttpPostObjectRequestParserDelegate <NSObject>

- (NSData *) parseObject:(id) theObject sourceURL:(NSURL *)theURL error:(NSError **)theError;

@end

@interface KVHttpPostObjectRequest : KVAbstractHttpRequest


- (id) initWithURL:(NSURL *)theURL postObject:(id)theObject postBodyObjectParser:(id<KVHttpPostObjectRequestParserDelegate>)thePostBodyObjectParser;

- (id) initWithURL:(NSURL *)theURL  parser:(id<KVASIHttpParseDelegate>)theParser postObject:(id)theObject postBodyObjectParser:(id<KVHttpPostObjectRequestParserDelegate>)thePostBodyObjectParser;


@property (nonatomic, readonly) id postObject;

@end


