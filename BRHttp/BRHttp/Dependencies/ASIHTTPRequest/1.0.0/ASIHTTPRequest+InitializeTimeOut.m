//
//  ASIHTTPRequest+InitializeTimeOut.m
//  KVFoundation
//
//  Created by Matt Preston on 05/04/2012.
//  Copyright (c) 2012 Knowledgeview Ltd. All rights reserved.
//

#import "ASIHTTPRequest+InitializeTimeOut.h"


/**
 * The default timeout in ASIHTTPRequest is 10s.  This is suitable for desktop applications, but Apple recommends 60s for 
 * mobile apps, which is the default when using NSURLRequest. See Bug 7832
 */
__attribute__((constructor)) static void initialize_ASIHTTPRequest_timeOut()
{
    @autoreleasepool 
    {
        [ASIHTTPRequest setDefaultTimeOutSeconds:60.0];
    }
}


@implementation ASIHTTPRequest (InitializeTimeOut)
@end
