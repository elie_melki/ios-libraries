//
//  KVObjectSerializer.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRObjectSerialization : NSObject<BRSerialization>

+ (BRObjectSerialization *) objectSerializationWith:(Class)theClass propertiesMapper:(NSDictionary *)thePropertiesMapper;

- (id) initWithClass:(Class)theClass propertiesMapper:(NSDictionary *)thePropertiesMapper;

@property (nonatomic, readonly) Class classType;
@property (nonatomic,readonly) NSDictionary *propertiesMapper;
@property (nonatomic,readonly) NSDictionary *serializers;


- (void) addSerialization:(id<BRSerialization>)theSerialization forPropertyName:(NSString *)thePropertyName;
- (void) removeSerializationForPropertyName:(NSString *)thePropertyName;



@end
