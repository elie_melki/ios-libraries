//
//  KVXMLHTTPRequest.h
//  KVFoundation
//
//  Created by Elie Melki on 19/08/2012.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"



extern NSString * const NSASIHTTPRequestParsedResultUserInfoKey;
extern NSString * const NSASIHTTPRequestParseErrorUserInfoKey;


@protocol KVASIHttpParseDelegate <NSObject>
- (id) parseObjectFromData:(NSData *)theData
                        source:(NSURL *)theSourceURL
                         error:(NSError **)theError;
@end



@interface KVASIHttpRequest : ASIHTTPRequest
{
}

/**
 * Used to configure the KVParseDelegate for post download processing.
 * If this value is not set,no post processing takes place.
 */
@property (assign) id<KVASIHttpParseDelegate> parseDelegate;


/**
 * Enable MD5 comparison of the response with a known previous value.
 */
@property (assign) BOOL useMD5Comparisons;


/**
 * The MD5 sum of the previous response
 */
@property (retain) NSString *previousResponseMD5;

/**
 * The MD5 sum of the response
 */
@property (retain) NSString *responseMD5;


/**
 * Checks to see if the response was loaded from the cache, or if the downloaded response had an
 * identical MD5 sum to a previously known value
 */
- (BOOL) didUseCachedResponseOrMD5Identical;


/**
 * Shortcut methods for reading the result of the parser processing from  userInfo
 */

@property (readonly, nonatomic) NSError *parserError;
@property (readonly, nonatomic) id parserObject;

@end
