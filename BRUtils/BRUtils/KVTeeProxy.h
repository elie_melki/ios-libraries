//
//  KVTeeProxy.h
//  KVFoundation
//
//  Created by Matt Preston on 14/01/2011.
//  Copyright 2011 Knowledgeview Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * A Proxy that dispatches method invocations to 2 wrapped objects
 * 
 * If either of the wrapped objects implements a particular selector, then this proxy will
 * report that it does as well.  
 *
 * Calling a method on this object the method, it will cause it to be invoked on each
 * of the wrapped objects.
 *
 * If only one of the wrapped objects implements the selector, the the method will only be 
 * invoked once.
 *
 * Calling retain/release on this class will cause those methods to get proxied on to the wrapped
 * objects.  To reclaim the memory used by this NSProxy object itself, call dealloc directly.
 *
 *
 * === WARNINGS ===
 *
 * Be careful with methods that have pass by reference variables that get modified. If both wrapped
 * objects respond to the selector, they can both modify the arguement.
 *
 * Be careful with methods that have return values. If both wrapped objects respond to the selector,
 * although both methods will be called, the second one will win and that will be the result returned 
 * from the proxy.
 */
@interface KVTeeProxy : NSProxy 
{
}

+ (id) teeProxyWithFirst:(id)theFirst second:(id)theSecond;

- (id) initWithFirst:(id)theFirst second:(id)theSecond;

@property (assign, nonatomic) id first;
@property (assign, nonatomic) id second;

@end
