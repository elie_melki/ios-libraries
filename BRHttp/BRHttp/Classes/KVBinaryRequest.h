//
//  RWBinaryRequest.h
//  Rewardisement
//
//  Created by ELie Melki on 11/28/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVHttpCachableRequest.h"

@class KVBinaryRequest;

@protocol NSBinaryRequestDelegate <KVAbstractHttpRequestDelegate>

- (void) binaryRequest:(KVBinaryRequest *)theRequest didRecieveBinaryAtPath:(NSString *)thePath;

@end



@interface KVBinaryRequest : KVHttpCachableRequest

@property (assign, nonatomic) id<NSBinaryRequestDelegate> delegate;

@end

