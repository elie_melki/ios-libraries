# This script is based on Jacob Van Order's answer on apple dev forums https://devforums.apple.com/message/971277
# See also http://spin.atomicobject.com/2011/12/13/building-a-universal-framework-for-ios/ for the start


# To get this to work with a Xcode 6 Cocoa Touch Framework, create Framework
# Then create a new Aggregate Target. Throw this script into a Build Script Phrase on the Aggregate


######################
# Options
######################



FRAMEWORK_NAME="${PROJECT_NAME}"

SIMULATOR_LIBRARY_PATH="${BUILD_DIR}/${CONFIGURATION}-iphonesimulator/${FRAMEWORK_NAME}.framework"

DEVICE_LIBRARY_PATH="${BUILD_DIR}/${CONFIGURATION}-iphoneos/${FRAMEWORK_NAME}.framework"

UNIVERSAL_LIBRARY_DIR="${BUILD_DIR}/${CONFIGURATION}-iphoneuniversal"

FRAMEWORK="${UNIVERSAL_LIBRARY_DIR}/${FRAMEWORK_NAME}.framework"


######################
# Build Frameworks
######################

xcodebuild -project ${PROJECT_NAME}.xcodeproj -sdk iphonesimulator -target ${PROJECT_NAME} ONLY_ACTIVE_ARCH=NO  -configuration ${CONFIGURATION} clean build CONFIGURATION_BUILD_DIR=${BUILD_DIR}/${CONFIGURATION}-iphonesimulator || exit 1

xcodebuild -project ${PROJECT_NAME}.xcodeproj -sdk iphoneos -target ${PROJECT_NAME} ONLY_ACTIVE_ARCH=NO  -configuration ${CONFIGURATION} clean build CONFIGURATION_BUILD_DIR=${BUILD_DIR}/${CONFIGURATION}-iphoneos || exit 1

#xcodebuild -target ${PROJECT_NAME} ONLY_ACTIVE_ARCH=NO -configuration ${CONFIGURATION} -sdk iphoneos  BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}" || exit 1

#xcodebuild -target ${PROJECT_NAME} ONLY_ACTIVE_ARCH=NO -configuration ${CONFIGURATION} -sdk iphonesimulator  BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}" || exit 1


######################
# Create directory for universal
######################

rm -rf "${UNIVERSAL_LIBRARY_DIR}"

mkdir "${UNIVERSAL_LIBRARY_DIR}"

mkdir "${FRAMEWORK}"


######################
# Copy files Framework
######################

cp -r "${DEVICE_LIBRARY_PATH}/." "${FRAMEWORK}"


######################
# Make fat universal binary
######################

lipo "${SIMULATOR_LIBRARY_PATH}/${FRAMEWORK_NAME}" "${DEVICE_LIBRARY_PATH}/${FRAMEWORK_NAME}" -create -output "${FRAMEWORK}/${FRAMEWORK_NAME}" | echo



######################
# Extract version
######################

BUILD_PLIST="${PROJECT}/Info.plist"
CURRENT_PROJECT_VERSION="$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "$BUILD_PLIST")"
CURRENT_PROJECT_VERSION_STRING="$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "$BUILD_PLIST")"




######################
# Add imports to all header files in project.h
######################

cd "${FRAMEWORK}/Headers"
#RESOURCE_LIST=$(ls `echo "*.h"`)
#while read -r filename; do
#if [ "${filename}" != "${PROJECT_NAME}.h" ]; then
#echo "#import <${PROJECT_NAME}/${filename}>" >> "${PROJECT_NAME}.h"
#fi
#done <<< "${RESOURCE_LIST}"

######################
# Replace symbols in .h
######################

NOW=`date`

sed -i '' s/'${PROJECT_VERSION}'/"${CURRENT_PROJECT_VERSION}"/g "${PROJECT_NAME}.h"
sed -i '' s/'${PROJECT_VERSION_STRING}'/"${CURRENT_PROJECT_VERSION_STRING}"/g "${PROJECT_NAME}.h"
sed -i '' s/'${BUILD_USER}'/"${USER}"/g "${PROJECT_NAME}.h"
sed -i '' s/'${BUILD_DATE}'/"${NOW}"/g "${PROJECT_NAME}.h"

open "${UNIVERSAL_LIBRARY_DIR}/"
