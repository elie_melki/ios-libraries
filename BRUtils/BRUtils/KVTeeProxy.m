//
//  KVTeeProxy.m
//  KVFoundation
//
//  Created by Matt Preston on 14/01/2011.
//  Copyright 2011 Knowledgeview Ltd. All rights reserved.
//

#import "KVTeeProxy.h"

@implementation KVTeeProxy

+ (id) teeProxyWithFirst:(id)theFirst second:(id)theSecond
{
    return [[[self alloc] initWithFirst:theFirst second:theSecond] autorelease];
}


@synthesize first, second;

- (id) initWithFirst:(id)theFirst second:(id)theSecond
{
    first = theFirst;
    second = theSecond;
    
    return self;
}

- (void) forwardInvocation:(NSInvocation *)theInvocation
{
    SEL _selector = [theInvocation selector];
    
    /**
     * respondsToSelector:(SEL) is a special method, we want it to return YES if
     * either of the wrapped objects implements the SEL arguement
     */
    
    if(_selector == @selector(respondsToSelector:))
    {
        /**
         * Step 1, fond the SEL arguement to the respondsToSelector: invocation.
         * Method args always start at index 2.
         */
        
        SEL _targetSelectorArg;
        [theInvocation getArgument:&_targetSelectorArg atIndex:2];
        
        /**
         * Check both nested objects to see if either respond to the selector.
         */
        
        if([first respondsToSelector:_targetSelectorArg])
        {
            [theInvocation invokeWithTarget:first];
        }
        else
        {
            [theInvocation invokeWithTarget:second];
        }
    }
    else
    {
        /**
         * For all other method invocations, simply invoke with both wrapped
         * objects if they respond to it.
         * 
         * If the invocation has a return value and both objects respond to the 
         * selector, the second one wins.
         */
        
        if([first respondsToSelector:_selector])
        {
            [theInvocation invokeWithTarget:first];
        }
        
        if([second respondsToSelector:_selector])
        {
            [theInvocation invokeWithTarget:second];
        }
    }
}

- (NSMethodSignature *) methodSignatureForSelector:(SEL)theSelector
{
    NSMethodSignature *_sig = [first methodSignatureForSelector:theSelector];
    
    if(!_sig)
    {
        _sig = [second methodSignatureForSelector:theSelector];
    }
    
    return _sig;
}

@end
