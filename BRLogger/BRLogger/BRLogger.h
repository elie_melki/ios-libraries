//
//  BRLogger.h
//  BRLogger
//
//  Created by ELie Melki on 10/19/14.
//  Copyright (c) 2014 My Company. All rights reserved.
//

//  Version: ${PROJECT_VERSION_STRING} (Build ${PROJECT_VERSION})
//
//  Built by: ${BUILD_USER}
//        at: ${BUILD_DATE}

#import <Foundation/Foundation.h>

//! Project version number for BRLogger.
FOUNDATION_EXPORT double BRLoggerVersionNumber;

//! Project version string for BRLogger.
FOUNDATION_EXPORT const unsigned char BRLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BRLogger/PublicHeader.h>



#ifndef BRLogTrace
#define BRLogTrace(fmt, ...) [[BRLogger sharedInstance] logTrace:[NSString stringWithFormat:@"[%@] %s[Line %d] %@", [NSThread currentThread], __PRETTY_FUNCTION__, __LINE__, fmt], ##__VA_ARGS__]
#endif

#ifndef BRLogInfo
#define BRLogInfo(fmt, ...) [[BRLogger sharedInstance] logInfo:[NSString stringWithFormat:@"[%@] %s[Line %d] %@", [NSThread currentThread], __PRETTY_FUNCTION__, __LINE__, fmt], ##__VA_ARGS__]
#endif

#ifndef BRLogError
#define BRLogError(fmt, ...) [[BRLogger sharedInstance] logError:[NSString stringWithFormat:@"[%@] %s[Line %d] %@", [NSThread currentThread], __PRETTY_FUNCTION__, __LINE__, fmt], ##__VA_ARGS__]
#endif

#ifndef BRLogDebug
#define BRLogDebug(fmt, ...) [[BRLogger sharedInstance] logDebug:[NSString stringWithFormat:@"[%@] %s[Line %d] %@", [NSThread currentThread], __PRETTY_FUNCTION__, __LINE__, fmt], ##__VA_ARGS__]
#endif


@protocol BRLoggerConfiguration <NSObject>

@property (nonatomic,readonly) BOOL isLoggingEnabled;
@property (nonatomic,readonly) BOOL isLoggingLevelTraceEnabled;
@property (nonatomic,readonly) BOOL isLoggingLevelInfoEnabled;
@property (nonatomic,readonly) BOOL isLoggingLevelErrorEnabled;
@property (nonatomic,readonly) BOOL isLoggingLevelDebugEnabled;



@end

@interface BRLogger : NSObject


+ (BRLogger *) sharedInstance;

+ (void) loadLogsWithConfig:(id<BRLoggerConfiguration>)theConfiguration;


- (void ) logDebug:(NSString *)format, ...; 
- (void ) logTrace:(NSString *)format, ...;
- (void ) logInfo:(NSString *)format, ... ;
- (void ) logError:(NSString *)format, ...;



@end




