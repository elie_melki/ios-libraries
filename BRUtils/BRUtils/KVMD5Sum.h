//
//  KVMD5Sum.h
//  KVFoundation
//
//  Created by Matt Preston on 26/07/2010.
//  Copyright 2010 Knowledgeview Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Calculates the MD5 sum for large files.
 */
@interface KVMD5Sum : NSObject 
{
}

+ (NSString *) stringMD5FromFile:(NSString *)thePath;

@end


@interface NSString (MD5)
- (NSString *) stringMD5;
@end

@interface NSURL (MD5)
- (NSString *) stringMD5;
@end

@interface NSData (MD5)
- (NSString *) stringMD5;
@end