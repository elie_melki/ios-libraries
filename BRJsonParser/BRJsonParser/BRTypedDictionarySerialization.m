//
//  BRTypedDictionarySerialization.m
//  BRJsonParser
//
//  Created by ELie Melki on 2/1/15.
//  Copyright (c) 2015 My. All rights reserved.
//

#import "BRTypedDictionarySerialization.h"

@interface BRTypedDictionarySerialization()

@property (nonatomic, retain) id<BRSerialization> valueSerializer;;

@end

@implementation BRTypedDictionarySerialization

@synthesize valueSerializer;

+ (BRTypedDictionarySerialization *) typedDictionarySerializationWith:(id<BRSerialization>)theSerializer;
{
    return [[[self alloc] initWithSerializer:theSerializer] autorelease];
}

- (id) initWithSerializer:(id<BRSerialization>)theSerializer
{
    self = [super init];
    if (self)
    {
        self.valueSerializer = theSerializer;
    }
    return self;
}


- (void) dealloc
{
    [valueSerializer release];
    [super dealloc];
}


//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *_r = [NSMutableDictionary dictionary];
        NSDictionary *_data = (NSDictionary *)theObject;
        for (NSString *_key in [_data allKeys])
        {
            id _value = [_data objectForKey:_key];
            id _serializedElement = [self.valueSerializer serialize:_value];
            [_r setObject:_serializedElement forKey:_key];
        }
        return _r;
    }
    else
    {
        //Todo throw an exception;
        return nil;
    }
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *_r = [NSMutableDictionary dictionary];
        NSDictionary *_data = (NSDictionary *)theData;
        for (NSString *_key in [_data allKeys])
        {
            id _value = [_data objectForKey:_key];
            id _deserializedElement = [self.valueSerializer deserialize:_value];
            [_r setObject:_deserializedElement forKey:_key];
        }
                return _r;
    }
    else  if (theData != nil && ![theData isKindOfClass:[NSNull class]])
    {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"%s[Line %d]: Expected a dictionary",__PRETTY_FUNCTION__, __LINE__]
                                     userInfo:nil];
    }
    return nil;
}

@end


