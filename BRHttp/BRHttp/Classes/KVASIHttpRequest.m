//
//  KVXMLHTTPRequest.m
//  KVFoundation
//
//  Created by Elie Melki on 19/08/2012.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVASIHTTPRequest.h"
#import <BRUtils/BRUtils.h>


NSString * const NSASIHTTPRequestParsedResultUserInfoKey = @"NSASIHTTPRequestParsedResultUserInfoKey";
NSString * const NSASIHTTPRequestParseErrorUserInfoKey   = @"NSASIHTTPRequestParseErrorUserInfoKey";


@interface KVASIHttpRequest()
- (NSMutableDictionary *) mutableUserInfo;
- (NSString *) calculateResponseMD5;
- (NSData *) dataFromResponse;
- (BOOL) isErrorStatusCode;
- (void) parseResponse;
@property (retain) NSRecursiveLock *cancelledLock;
@end



@implementation KVASIHttpRequest

@synthesize parseDelegate, useMD5Comparisons, responseMD5, previousResponseMD5;
@dynamic cancelledLock;

- (void) dealloc
{
    [previousResponseMD5 release];
    [responseMD5 release];

    [super dealloc];
}

//-------------------------------------
// Public interface
//-------------------------------------
#pragma mark - Public interface

- (void) clearDelegatesAndCancel
{
    [[self cancelledLock] lock];
    
    self.parseDelegate = nil;
    
    [super clearDelegatesAndCancel];
    
    [[self cancelledLock] unlock];
}

- (void) requestFinished
{
    if([self error])
    {
        return;
    }
    
    if ([self isErrorStatusCode])
    {
        NSError *_error = [NSError errorWithDomain:self.responseStatusMessage code:self.responseStatusCode userInfo:nil];
        [self failWithError:_error];
        return;
    }
    
    if(self.useMD5Comparisons)
    {
        self.responseMD5 = [self calculateResponseMD5];
    }
    
    [self parseResponse];
      
    [super requestFinished];
}


//-------------------------------------
// MD5 comparisons
//-------------------------------------
#pragma mark - MD5 comparisons

- (BOOL) didUseCachedResponseOrMD5Identical
{
    if(self.useMD5Comparisons)
    {
        return self.didUseCachedResponse || [self.previousResponseMD5 isEqualToString:self.responseMD5];
    }
    else
    {
        return self.didUseCachedResponse;
    }
}


//-------------------------------------
// XML Processing results
//-------------------------------------
#pragma mark - XML Processing results

- (NSError *) parserError
{
    return [[self userInfo] objectForKey:NSASIHTTPRequestParseErrorUserInfoKey];
}

- (id) parserObject
{
    return [[self userInfo] objectForKey:NSASIHTTPRequestParsedResultUserInfoKey];
}


//-------------------------------------
// Private interface
//-------------------------------------
#pragma mark - Private interface

- (void) parseResponse
{
    id<KVASIHttpParseDelegate> _parseDelegate;
    
    /**
     * As we're running in a non main thread at this point, get a reference to
     * the parse delegate under a lock.
     */
    [[self cancelledLock] lock];
    
    _parseDelegate = [self parseDelegate];
    
    if(_parseDelegate)
    {
        // In case clearDelegatesAndCancel is called while we are parsing the document
        [_parseDelegate retain];
        
        // Prevents 'us' being deallocated mid xml processing
        [self retain];
    }
    
    [[self cancelledLock] unlock];
    
    
    if(_parseDelegate)
    {
        @autoreleasepool
        {
            NSMutableDictionary *_mutableUserInfo = [self mutableUserInfo];
            
            NSError *_error = NULL;
            
            NSData *_data = [self dataFromResponse];
            
            id _result = [_parseDelegate parseObjectFromData:_data source:[self originalURL] error:&_error];
            
            if(_error)
                [_mutableUserInfo setObject:_error forKey:NSASIHTTPRequestParseErrorUserInfoKey];
            else if (_result != nil)
                [_mutableUserInfo setObject:_result forKey:NSASIHTTPRequestParsedResultUserInfoKey];
            
            
            
            [_parseDelegate release];
            
            /**
             * In case we hold the last retain on 'us', don't want the object to be immediately deallocated
             */
            [self autorelease];
            
            self.userInfo = _mutableUserInfo;
        }
    }
}

- (BOOL) isErrorStatusCode
{
   NSInteger _sc = self.responseStatusCode;
   return (_sc >= 400) && (_sc <= 500);
}


- (NSData *) dataFromResponse
{
    NSData *_data = nil;
    
    NSString *_downloadPath = self.downloadDestinationPath;
    if(_downloadPath)
    {
        _data = [NSData dataWithContentsOfFile:_downloadPath];
    }
    else
    {
        _data = [self responseData];
    }
    
    return _data;
}

- (NSMutableDictionary *) mutableUserInfo
{
    NSDictionary *_userInfo = [self userInfo];
    
    if(_userInfo)
    {
        return [NSMutableDictionary dictionaryWithDictionary:_userInfo];
    }
    else
    {
        return [NSMutableDictionary dictionary];
    }
}

- (NSString *) calculateResponseMD5
{
    NSData *_data = [self responseData];
    
    if(_data)
    {
        return [_data stringMD5];
    }
    else
    {
        return [KVMD5Sum stringMD5FromFile:self.downloadDestinationPath];
    }
}

@end
