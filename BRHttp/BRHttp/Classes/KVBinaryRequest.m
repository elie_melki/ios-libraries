//
//  RWBinaryRequest.m
//  Rewardisement
//
//  Created by ELie Melki on 11/28/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVBinaryRequest.h"
#import "KVAbstractHttpRequest+Protected.h"

@interface NSOperationQueue (ADRequest)
+ (NSOperationQueue *) binaryQueue;
@end


@implementation KVBinaryRequest

@synthesize delegate;

//-------------------------------------
// Init & Dealloc
//-------------------------------------
#pragma mark - Init & Dealloc


//------------------------------
//Ovveride Methods
//------------------------------
#pragma mark - Ovveride Methods

- (void) doSomethingWithValue:(id)theValue
{
    [self.delegate binaryRequest:self didRecieveBinaryAtPath:theValue];
}

- (NSOperationQueue *) queue
{
    return [NSOperationQueue binaryQueue];
}


@end


//-------------------------------------
// NSOperationQueue Impl
//-------------------------------------
#pragma mark - NSOperationQueue Impl

@implementation NSOperationQueue (ADRequest)

+ (NSOperationQueue *) binaryQueue
{
    static NSOperationQueue *queue;
    
    if(!queue)
    {
        queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount = 10;
    }
    
    return queue;
}
@end
