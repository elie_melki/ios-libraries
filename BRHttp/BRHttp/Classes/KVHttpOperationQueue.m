//
//  KVHttpOperationQueue.m
//  KVHttpRequest
//
//  Created by ELie Melki on 11/19/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVHttpOperationQueue.h"


@implementation KVHttpOperationQueue

@synthesize delegate;

- (void) dealloc
{
    [_userInfo release];
    [super dealloc];
}

- (void) addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait
{
    for (NSOperation *op in ops)
    {
        [op addObserver:self forKeyPath:@"isFinished" options:0 context:NULL];
    }
    [super addOperations:ops waitUntilFinished:wait];
}



- (void) addOperation:(NSOperation *)TheOperation
{
    [TheOperation addObserver:self forKeyPath:@"isFinished" options:0 context:NULL];
    [super addOperation:TheOperation];
}

- (void)observeValueForKeyPath:(NSString *)theKeyPath ofObject:(id)theObject change:(NSDictionary *)theChange context:(void *)theContext
{
    NSAssert( [theKeyPath isEqualToString:@"isFinished"],@"Should only be registered ");
    NSAssert([NSThread isMainThread],@"KVHttpOperationQueue:observeValueForKeyPath Should Only Run on the Main Thread");
    NSAssert([theObject isKindOfClass:[NSOperation class]],@"KVHttpOperationQueue:observeValueForKeyPath should be of type nsoperation");
    if (self.operationCount == 0)
    {
        if ([self.delegate respondsToSelector:@selector(queueComplete:)])
            [self.delegate queueComplete:self];
    }
    NSOperation *_op = (NSOperation *)theObject;
    
    if ([_op isFinished])
    {
        [_op removeObserver:self forKeyPath:theKeyPath];
    }
    

}

@end
