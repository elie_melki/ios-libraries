//
//  BRUtils.h
//  BRUtils
//
//  Created by ELie Melki on 10/19/14.
//  Copyright (c) 2014 My Company. All rights reserved.
//

//  Version: ${PROJECT_VERSION_STRING} (Build ${PROJECT_VERSION})
//
//  Built by: ${BUILD_USER}
//        at: ${BUILD_DATE}


#import <Foundation/Foundation.h>

//! Project version number for BRUtils.
FOUNDATION_EXPORT double BRUtilsVersionNumber;

//! Project version string for BRUtils.
FOUNDATION_EXPORT const unsigned char BRUtilsVersionString[];


// In this header, you should import all the public headers of your framework using statements like #import <BRUtils/PublicHeader.h>

#import <BRUtils/OrderedDictionary.h>
#import <BRUtils/KVMD5Sum.h>
#import <BRUtils/KVTeeProxy.h>
#import <BRUtils/JRSwizzle.h>
