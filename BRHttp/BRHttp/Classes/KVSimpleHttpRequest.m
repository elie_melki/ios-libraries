//
//  NSSimpleHttpRequest.m
//  NSFoundation
//
//  Created by ELie Melki on 3/17/13.
//  Copyright (c) 2013 ELie Melki. All rights reserved.
//

#import "KVSimpleHttpRequest.h"

@implementation KVSimpleHttpRequest

@synthesize delegate;

- (void) doSomethingWithValue:(id)theValue
{
    if ([self.delegate respondsToSelector:@selector(simpleHttpRequest:didRecieveData:)])
    {
        NSData *_dt = [[NSData alloc] initWithContentsOfFile:theValue];
        [self.delegate simpleHttpRequest:self didRecieveData:_dt];
        [_dt release];
    }

}

@end
