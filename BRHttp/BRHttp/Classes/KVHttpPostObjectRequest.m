//
//  KVHttpPostRequest.m
//  Rewardisement
//
//  Created by ELie Melki on 11/29/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVHttpPostObjectRequest.h"
#import "KVAbstractHttpRequest+Protected.h"

@interface KVHttpPostObjectRequest()

@property (nonatomic,retain) id<KVHttpPostObjectRequestParserDelegate> postBodyObjectParser;

@end

@implementation KVHttpPostObjectRequest

@synthesize postObject,postBodyObjectParser;

- (id) initWithURL:(NSURL *)theURL postObject:(id)theObject postBodyObjectParser:(id<KVHttpPostObjectRequestParserDelegate>)thePostBodyObjectParser
{
    return [self initWithURL:theURL parser:nil postObject:theObject postBodyObjectParser:thePostBodyObjectParser];
}

- (id) initWithURL:(NSURL *)theURL parser:(id<KVASIHttpParseDelegate>)theParser  postObject:(id)theObject postBodyObjectParser:(id<KVHttpPostObjectRequestParserDelegate>)thePostBodyObjectParser
{
    self = [super initWithURL:theURL parser:theParser];
    if (self)
    {
        postObject = [theObject retain];
        postBodyObjectParser = [thePostBodyObjectParser retain];
    }
    return self;

    
}


- (void) dealloc
{
    [postBodyObjectParser release];
    [postObject release];
    [super dealloc];
}


- (void) startExecutingRequest
{
    NSError *_error = NULL;
    NSData *_data = [postBodyObjectParser parseObject:postObject sourceURL:self.URL error:&_error];
    if (_error)
    {
        if ([self.delegate respondsToSelector:@selector(KVRequest:didFailWithError:)])
        {
            [self.delegate KVRequest:self didFailWithError:_error];
        }
        return;
    }
    else
    {
        [operation release];
        operation = [[self KVASIHttpRequestWithURL:self.URL withData:_data] retain];
        [self.queue addOperation:operation];
    }
    
}
- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL withData:(NSData *)theData
{
      KVASIHttpRequest *_request = [super KVASIHttpRequestWithURL:theURL];
    [_request setRequestMethod:@"POST"];
    if (theData != nil)
        [_request setPostBody:[NSMutableData dataWithData:theData]];
    return _request;
}

@end