//
//  KVLocaleSerialization.h
//  Rewardisement
//
//  Created by ELie Melki on 11/23/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRLocaleSerialization : NSObject<BRSerialization>

+ (BRLocaleSerialization *) localeSerialization;

@end
