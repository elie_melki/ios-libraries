//
//  KVMD5Sum.m
//  KVFoundation
//
//  Created by Matt Preston on 26/07/2010.
//  Copyright 2010 Knowledgeview Ltd. All rights reserved.
//

#import "KVMD5Sum.h"
#import <CommonCrypto/CommonDigest.h>

@implementation KVMD5Sum

+ (NSString *) stringMD5FromDigest:(unsigned char *)theDigest
{
    return [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                                       theDigest[0], theDigest[1], 
                                       theDigest[2], theDigest[3],
                                       theDigest[4], theDigest[5],
                                       theDigest[6], theDigest[7],
                                       theDigest[8], theDigest[9],
                                       theDigest[10], theDigest[11],
                                       theDigest[12], theDigest[13],
                                       theDigest[14], theDigest[15]];
}

+ (NSString *) stringMD5FromFile:(NSString *)thePath;
{
    NSFileHandle *_handle = [NSFileHandle fileHandleForReadingAtPath:thePath];
    
    if(_handle == nil)
    {
        return nil; // file didnt exist - TODO, proper error handling
    }
        
    CC_MD5_CTX _md5;
    CC_MD5_Init(&_md5);
	
    BOOL _done = NO;
	
    while(!_done)
    {
        NSData *_fileData = [[NSData alloc] initWithData:[_handle readDataOfLength:4096]];

        CC_MD5_Update(&_md5, [_fileData bytes], [_fileData length]);
		
        if([_fileData length] == 0) 
        {
            _done = YES;
        }
    
        [_fileData release];
    }
    
    [_handle closeFile];
    
    unsigned char _digest[CC_MD5_DIGEST_LENGTH];
	
    CC_MD5_Final(_digest, &_md5);
	
    return [self stringMD5FromDigest:_digest];
}

@end


@implementation NSString (MD5)

- (NSString *) stringMD5
{
    const char *_cString = [self UTF8String];
    unsigned char _digest[16];
    
    CC_MD5(_cString, strlen(_cString), _digest);
    
    return [KVMD5Sum stringMD5FromDigest:_digest];
}

@end


@implementation NSURL (MD5)

- (NSString *) stringMD5
{
    return [[self absoluteString] stringMD5];
}

@end


@implementation NSData (MD5)

- (NSString *) stringMD5
{    
    unsigned char _digest[16];
    
    CC_MD5([self bytes], [self length], _digest);
    
    return [KVMD5Sum stringMD5FromDigest:_digest];
}

@end
