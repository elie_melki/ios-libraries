//
//  KVURLSerializer.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRAbsoluteURLSerialization : NSObject<BRSerialization>

+ (BRAbsoluteURLSerialization *) absoluteURLSerialization;

@end
