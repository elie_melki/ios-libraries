//
//  KVAbstractHttpRequest.h
//  Rewardisement
//
//  Created by ELie Melki on 11/27/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KVASIHttpRequest.h"

@class KVAbstractHttpRequest;

@protocol KVAbstractHttpRequestDelegate <NSObject>

@optional

- (void) KVRequestDidFinish:(KVAbstractHttpRequest *)theRequest;
- (void) KVRequest:(KVAbstractHttpRequest *)theRequest didUpdateDownloadProgress:(float)theProgress;
- (void) KVRequest:(KVAbstractHttpRequest *)theRequest didFailWithError:(NSError *)theError;

@end


@interface KVAbstractHttpRequest : NSOperation
{
    KVASIHttpRequest *operation;
}

- (id) initWithURL:(NSURL *)theURL;
- (id) initWithURL:(NSURL *)theURL parser:(id<KVASIHttpParseDelegate>) theParser;

- (void) start;
- (void) cancel;
- (void) cancelAndClearDelegate;

@property (nonatomic,readonly)  BOOL requestInProgress;
@property (nonatomic,assign)    id<KVAbstractHttpRequestDelegate> delegate;
@property (nonatomic,retain)    NSDictionary *userInfo;
@property (nonatomic,assign)    NSInteger tag;
@property (nonatomic,readonly)  id<ASICacheDelegate> cacheStorage;
@property (nonatomic,readonly)  NSURL *URL;
@property (nonatomic,readonly) id<KVASIHttpParseDelegate> parser;

@end



