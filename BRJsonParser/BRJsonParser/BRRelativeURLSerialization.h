//
//  KVRelativeURLSerialization.h
//  Rewardisement
//
//  Created by ELie Melki on 11/28/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRRelativeURLSerialization : NSObject<BRSerialization>

+ (BRRelativeURLSerialization *) relativeSerializationWithBaseURL:(NSURL *)theBaseURL;

- (id) initWithBaseURL:(NSURL *)theBaseURL;

@end
