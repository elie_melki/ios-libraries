//
//  NSObject+Properties.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>


@interface KVProperty : NSObject

@property (nonatomic,readonly) NSString *propertyType;
@property (nonatomic,readonly) NSString *propertyName;

@end

@interface NSObject (Properties)

- (NSArray *) properties;
- (KVProperty *) propertyWithName:(NSString *)theName;
- (KVProperty *) propertyFor:(objc_property_t)theProperty;

@end
