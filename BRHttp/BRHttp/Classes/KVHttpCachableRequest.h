//
//  KVHttpCachableRequest.h
//  Rewardisement
//
//  Created by ELie Melki on 11/27/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KVAbstractHttpRequest.h"

@interface KVHttpCachableRequest : KVAbstractHttpRequest
{
    @protected
     BOOL needsRefresh;
}
- (void) setNeedsRefresh;
@property (nonatomic) BOOL useMD5Comparisons;

@end

