//
//  KVAbstractHttpRequest.m
//  Rewardisement
//
//  Created by ELie Melki on 11/27/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVAbstractHttpRequest.h"
#import "ASIDownloadCache.h"
#import "KVAbstractHttpRequest+Protected.h"

@interface NSOperationQueue (ADRequest)
+ (NSOperationQueue *) requestQueue;
@end

@interface KVAbstractHttpRequest() <ASIHTTPRequestDelegate, ASIProgressDelegate>

@property (nonatomic,retain) id<KVASIHttpParseDelegate> parser;
@property (nonatomic) BOOL executing;
@property (nonatomic) BOOL finished;
@property (nonatomic) BOOL canceled;

- (void) startExecuting;
- (void) cancelExecuting;
- (void) finishExecuting;

- (void) cancelOperation;
- (void) releaseOperation:(ASIHTTPRequest *)theRequest;
- (void) handleFileURL;

@end

@implementation KVAbstractHttpRequest


@dynamic delegate;
@synthesize userInfo, tag;
@synthesize executing,finished,canceled;
@synthesize URL;


//-------------------------------------
// Init & Dealloc
//-------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithURL:(NSURL *)theURL
{
    self = [self initWithURL:theURL parser:nil];
    
    if(self)
    {
       
    }
    
    return self;
}

- (id) initWithURL:(NSURL *)theURL parser:(id<KVASIHttpParseDelegate>)theParser
{
    self = [super init];
    
    if(self)
    {
        _parser = [theParser retain];
        URL = [theURL copy];
    }
    
    
    return self;
}

- (void) dealloc
{
    //Not sure of this maybe this should be [self cancelOperation];
    [self cancel];
    [_parser release];
    [URL release];
    [userInfo release];
    [super dealloc];
}

//-------------------------------------
// NSOperation Ovveride Methods
//-------------------------------------
#pragma mark - NSOperation Ovveride Methods

- (void) start
{
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
        return;
    }
    if(self.requestInProgress || self.isCancelled)
    {
        return; // Don't want concurrent requests
    }
    
    
    [self startExecuting];
    
    if ([self.URL isFileURL])
    {
        [self handleFileURL];
    }
    else
        [self startExecutingRequest];
    
}

- (void) cancel
{
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(cancel) withObject:nil waitUntilDone:YES];
        return;
    }
    else
    {
        [self cancelOperation];
        [self cancelExecuting];
        [super cancel];
    }
}

- (void) cancelAndClearDelegate
{
    self.delegate = nil;
    [self cancel];
}

- (BOOL) isExecuting
{
    return self.executing;
}

- (BOOL) isFinished
{
    return self.finished;
}

- (BOOL) isCancelled
{
    return self.canceled;
}

//-------------------------------------
// Public interface
//-------------------------------------
#pragma mark - Public interface

- (BOOL) requestInProgress
{
    return self.executing;
}

- (id<ASICacheDelegate>) cacheStorage
{
    return [ASIDownloadCache sharedCache];
}

//-------------------------------------
// Protected interface
//-------------------------------------
#pragma mark - Protected interface

- (void) startExecutingRequest
{
    KVASIHttpRequest *_request = [self KVASIHttpRequestWithURL:URL];
 
    [operation release];
    operation = [_request retain];
    
    [[NSOperationQueue requestQueue] addOperation:operation];
}


- (void) doSomethingWithValue:(id)theValue
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"doSomethingWithValue: should be implemented in a subclass"
                                 userInfo:nil];
}


- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL
{
    KVASIHttpRequest *_request =  [[KVASIHttpRequest alloc] initWithURL:theURL];
    
    _request.delegate = self;
    _request.downloadProgressDelegate = self;
    _request.timeOutSeconds = [KVASIHttpRequest defaultTimeOutSeconds];
    
    ASIDownloadCache *_cache = self.cacheStorage;
    
    _request.downloadCache = _cache;
    _request.cacheStoragePolicy = ASICachePermanentlyCacheStoragePolicy;
    
    _request.downloadDestinationPath = [_cache pathToCachedResponseDataForURL:theURL];
    
    _request.parseDelegate = [self parser];
    return [_request autorelease];
}

- (NSOperationQueue *) queue
{
    return [NSOperationQueue requestQueue];
}


- (void) executeSuccessRequest:(KVASIHttpRequest *)theRequest
{
    id _value = [self objectValueFromResponse:theRequest];
    
    [self doSomethingWithValue:_value];
    
    [self reportRequestDidFinish];
}

- (void) reportRequestComplete
{
    [self finishExecuting];
}

- (void) reportRequestFinishedWithError:(NSError *)theError
{
    // TODO - check for cancelled requests and ignore?
    if([self.delegate respondsToSelector:@selector(KVRequest:didFailWithError:)])
    {
        [self.delegate KVRequest:self didFailWithError:theError];
    }
    
    [self reportRequestComplete];
}

- (void) reportRequestDidFinish
{
    if([self.delegate respondsToSelector:@selector(KVRequestDidFinish:)])
    {
        [self.delegate KVRequestDidFinish:self];
    }
    [self reportRequestComplete];
}

//-------------------------------------
// Post download processing
//-------------------------------------
#pragma mark - Post download processing

- (NSError *) errorFromPostDownloadProcessing:(KVASIHttpRequest *)theRequest
{
    return theRequest.parseDelegate == nil ?  nil :  theRequest.parserError;
}

- (id) objectValueFromResponse:(KVASIHttpRequest *)theRequest
{
    if (theRequest.parseDelegate == nil)
    {
        NSString *_downloadPath = theRequest.downloadDestinationPath;
        
        if(!_downloadPath)
        {
            _downloadPath = [theRequest.downloadCache pathToCachedResponseDataForURL:theRequest.url];
        }
        return _downloadPath;
    }
    else
    {
       return theRequest.parserObject;
    }
}

//-------------------------------------
// Private interface
//-------------------------------------
#pragma mark - Private interface


//Todo switch to background thread
- (void) handleFileURL
{
     //Keep a back up copy in case we were released inside the delegate method impl
    [[self retain] autorelease];
    
    NSFileManager *_fileManager = [NSFileManager defaultManager];
    NSString *_path = [self.URL path];
    if ([_fileManager fileExistsAtPath:_path ])
    {
        id _result = _path;
        if (self.parser != nil)
        {
            NSData *_data = [NSData dataWithContentsOfURL:self.URL];
            NSError *_error = NULL;
            _result = [self.parser parseObjectFromData:_data source:self.URL error:&_error];
            
            if (_error)
            {
                [self reportRequestFinishedWithError:_error];
                return;
            }
        }
        [self doSomethingWithValue:_result];
        [self reportRequestDidFinish];
    }
    else
    {
        //TODO Add an error message
       [self reportRequestFinishedWithError:[NSError errorWithDomain:@"" code:0 userInfo:nil]];
    }
}


- (void) startExecuting
{
    self.finished = NO;
    self.canceled = NO;
    [self willChangeValueForKey:@"isExecuting"];
    self.executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void) finishExecuting
{
    self.executing = NO;
    self.canceled = NO;
    [self willChangeValueForKey:@"isFinished"];
    self.finished = YES;
    [self didChangeValueForKey:@"isFinished"];
}

- (void) cancelExecuting
{
    self.executing = NO;
    [self willChangeValueForKey:@"isCanceled"];
    self.canceled = YES;
    [self didChangeValueForKey:@"isCanceled"];
    [self willChangeValueForKey:@"isFinished"];
    self.finished = YES;
    [self didChangeValueForKey:@"isFinished"];
    
}

- (void) cancelOperation
{
    [operation clearDelegatesAndCancel];
    [self releaseOperation:operation];
}

- (void) releaseOperation:(ASIHTTPRequest *)theRequest
{
    [operation release];
    operation = nil;
}



//-------------------------------------
// ASIProgressDelegate
//-------------------------------------
#pragma mark - ASIProgressDelegate

- (void) setProgress:(float)newProgress
{
    if([self.delegate respondsToSelector:@selector(KVRequest:didUpdateDownloadProgress:)])
    {
        [self.delegate KVRequest:self didUpdateDownloadProgress:newProgress];
    }
}

//-------------------------------------
// ASIHTTPRequestDelegate
//-------------------------------------
#pragma mark - ASIHTTPRequestDelegate

- (void) requestFinished:(KVASIHttpRequest *)theRequest
{
     //Keep a back up copy in case we were released inside the delegate method impl
    [[self retain] autorelease];
    [self releaseOperation:theRequest];
    
    NSError *_error = [self errorFromPostDownloadProcessing:theRequest];
    
    if(_error)
    {
        [theRequest.downloadCache removeCachedDataForRequest:theRequest];
        [self reportRequestFinishedWithError:_error];
    }
    else
    {
        [self executeSuccessRequest:theRequest];
       
    }
}

- (void) requestFailed:(ASIHTTPRequest *)theRequest
{
     //Keep a back up copy in case we were released inside the delegate method impl
    [[self retain] autorelease];
    
    [self releaseOperation:theRequest];
    [self reportRequestFinishedWithError:theRequest.error];
}

@end



//-------------------------------------
// NSOperationQueue Impl
//-------------------------------------
#pragma mark - NSOperationQueue Impl

@implementation NSOperationQueue (ADRequest)

+ (NSOperationQueue *) requestQueue
{
    static NSOperationQueue *queue;
    
    if(!queue)
    {
        queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount = 10;
    }
    
    return queue;
}


@end
