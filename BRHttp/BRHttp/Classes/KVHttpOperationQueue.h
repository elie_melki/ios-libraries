//
//  KVHttpOperationQueue.h
//  KVHttpRequest
//
//  Created by ELie Melki on 11/19/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KVHttpOperationQueue;

@protocol KVHttpOperationQueueDelegate <NSObject>

@optional
- (void) queueComplete:(KVHttpOperationQueue *)theQueue;

@end


@interface KVHttpOperationQueue : NSOperationQueue

@property (nonatomic,assign) id<KVHttpOperationQueueDelegate> delegate;
@property (nonatomic,retain) NSDictionary *userInfo;
@end
