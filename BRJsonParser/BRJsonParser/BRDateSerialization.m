//
//  KVDateSerialization.m
//  Rewardisement
//
//  Created by ELie Melki on 11/23/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRDateSerialization.h"
@interface BRDateSerialization()

@property (nonatomic,readonly) NSDateFormatter *dateFormatter;
@end

@implementation BRDateSerialization

+ (BRDateSerialization *) dateSerializationWith:(NSString *)theDateFormat
{
    return [[[self alloc] initWithDateFormat:theDateFormat] autorelease];
}

@synthesize dateFormat;

 - (id) initWithDateFormat:(NSString *)theDateFormat
{
    self = [super init];
    if (self)
    {
        dateFormat = [theDateFormat retain];
    }
    return self;
}

- (void) dealloc
{
    [dateFormat release];
    [super dealloc];
}

//------------------------------------
//Private Methods
//------------------------------------
#pragma mark - Private Methods


- (NSDateFormatter *) dateFormatter
{
    NSDateFormatter *_fm = [[NSDateFormatter alloc] init];
    _fm.dateFormat = self.dateFormat;
    return [_fm autorelease];
}

//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:[NSDate class]])
        return [self.dateFormatter stringFromDate:theObject];
    else
        return nil;

    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSString class]])
        return [self.dateFormatter dateFromString:theData];
    else if (theData != nil && ![theData isKindOfClass:[NSNull class]])
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"%s[Line %d]: Expected an NSString for %@ found other %@",__PRETTY_FUNCTION__, __LINE__, theData, [theData class]]
                                     userInfo:nil];
    return nil;
}



@end
