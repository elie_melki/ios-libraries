//
//  KVLog.m
//  EditionsKit
//
//  Created by Elie Melki on 6/26/12.
//  Copyright (c) 2012 Elie Melki. All rights reserved.
//

#import "BRLogger.h"


static BRLogger *logInstance;

@interface BRLogger()

@property (nonatomic,retain) id<BRLoggerConfiguration> config;
- (NSString *) logDateFormat:(NSDate *)theDate format:(NSString *)theFormat;
- (void) clearOldLogs;

@end

@implementation BRLogger

@synthesize config;

+ (BRLogger *) sharedInstance
{
    return logInstance;
}

+ (void) loadLogsWithConfig:(id<BRLoggerConfiguration>)theConfiguration
{
    if (logInstance == nil)
    {
        if ([theConfiguration isLoggingEnabled])
        {
            logInstance = [[super allocWithZone:NULL] init];
            logInstance.config = theConfiguration;
      
        }
    }
}

- (void) logLevel:(NSString *)lvl format:(NSString *)format arguments:(va_list) arguments
{
    NSString *_format = [NSString stringWithFormat:@"%@ %@",lvl,format];
    NSLogv(_format, arguments);
}

- (void ) logDebug:(NSString *)format, ...
{
    if (!config.isLoggingLevelDebugEnabled)
        return;
    
    va_list args;
    va_start(args, format);
    [self logLevel:@"DEBUG" format:format arguments:args];
    va_end(args);
}

- (void ) logTrace:(NSString *)format, ...
{
    if (!config.isLoggingLevelTraceEnabled)
        return;
    
    va_list args;
    va_start(args, format);
    [self logLevel:@"TRACE" format:format arguments:args];
    va_end(args);
}

- (void ) logInfo:(NSString *)format, ... 
{
    if (!config.isLoggingLevelInfoEnabled)
        return;
    
    va_list args;
    va_start(args, format);
    [self logLevel:@"INFO" format:format arguments:args];
    va_end(args);
}

- (void ) logError:(NSString *)format, ... 
{
    if (!config.isLoggingLevelErrorEnabled)
        return;
    
    va_list args;
    va_start(args, format);
    [self logLevel:@"ERROR" format:format arguments:args];
    va_end(args);
}


- (void) dealloc
{
    [config release];
    [super dealloc];
}

+ (id) allocWithZone:(NSZone*)theZone
{
    return [[self sharedInstance] retain];
}

- (id) retain 
{
    return self;
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax; // denotes an object that cannot be released
}

- (oneway void) release 
{
    //do nothing
}

- (id) autorelease 
{
    return self;
}

//------------------------
//Private Function
//------------------------

#pragma - Private Functions

- (NSString *) logDateFormat:(NSDate *)theDate format:(NSString *)theFormat;
{
    NSDateFormatter *_df = [[NSDateFormatter alloc] init];
    [_df setDateFormat:theFormat];
    NSString *_dateString = [_df stringFromDate:theDate];
    [_df release];
    return _dateString;
    
}

- (void) clearOldLogs
{
    //TODO
}

@end
