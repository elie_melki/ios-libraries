//
//  KVObjectSerializer.m
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "BRObjectSerialization.h"
#import "NSObject+Properties.h"
#import <BRLogger/BRLogger.h>
#import <BRUtils/BRUtils.h>

@interface BRObjectSerialization()

@property (nonatomic,retain) NSMutableDictionary *mutableSerializers;

- (id<BRSerialization>) serializationForProperty:(NSString *)thePropertyName;

@end

@implementation BRObjectSerialization


+ (BRObjectSerialization *) objectSerializationWith:(Class)theClass propertiesMapper:(NSDictionary *)thePropertiesMapper
{
    return [[[self alloc] initWithClass:theClass propertiesMapper:thePropertiesMapper] autorelease];
}

@synthesize mutableSerializers,classType,propertiesMapper;

//------------------------------------
//Init & Dealloc
//------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithClass:(Class)theClass propertiesMapper:(NSDictionary *)thePropertiesMapper
{
    self = [super init];
    if (self)
    {
        classType = theClass;
        mutableSerializers = [NSMutableDictionary new];
        propertiesMapper = [thePropertiesMapper retain];
    }
    return self;
}

- (void) dealloc
{
    [propertiesMapper release];
    [mutableSerializers release];
    [super dealloc];
}


//------------------------------------
//Private Methods
//------------------------------------
#pragma mark - Private Methods

- (id<BRSerialization>) serializationForProperty:(NSString *)thePropertyName
{
    id<BRSerialization> _serializer = [mutableSerializers objectForKey:thePropertyName];
    
    return _serializer;
}


//------------------------------------
//Public Methods
//------------------------------------
#pragma mark - Public Methods

- (void) addSerialization:(id<BRSerialization>)theSerialization forPropertyName:(NSString *)thePropertyName
{
    [mutableSerializers setObject:theSerialization forKey:thePropertyName];
}

- (void) removeSerializationForPropertyName:(NSString *)thePropertyName
{
    [mutableSerializers removeObjectForKey:thePropertyName];
}

- (NSDictionary *) serializers
{
    return mutableSerializers;
}

//------------------------------------
//KVSerializer implementation
//------------------------------------
#pragma mark - KVSerializer implementation

- (id) serialize:(id)theObject
{
    if ([theObject isKindOfClass:self.classType])
    {
        OrderedDictionary *_jsonDic = [OrderedDictionary dictionary];
        for (NSString *_property in [propertiesMapper allKeys])
        {
            NSString *_key = [propertiesMapper objectForKey:_property];
           
            id _value = [theObject valueForKey:_property];
            id<BRSerialization> _serializer = [mutableSerializers objectForKey:_property];
            
            if (_serializer)
                _value = [_serializer serialize:_value];
            if (_value)
                [_jsonDic setObject:_value forKey:_key];
            else if (_value == nil)
                [_jsonDic setObject:[NSNull null] forKey:_key];
        }
        return _jsonDic;
    }
    else return nil;
    
}

- (id) deserialize:(id)theData
{
    if ([theData isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *_json = (NSDictionary *)theData;
        id _instance = [[[classType alloc] init] autorelease];
       
        for (NSString *_property in [propertiesMapper allKeys])
        {
      
            NSString *_jsonKey = [propertiesMapper objectForKey:_property];
        
            id _propertyValue = [_json objectForKey:_jsonKey];
        
            if (_propertyValue == nil || [_propertyValue isKindOfClass:[NSNull class]])
            {
                BRLogInfo(@"Object Property [%@] Mapped to JsonKey [%@] For Class [%@] doesnt exist in Json.",_property,_jsonKey,classType);
            }
            else
            {
                id _serializer = [self serializationForProperty:_property];
                if (_serializer && _propertyValue)
                    _propertyValue = [_serializer deserialize:_propertyValue];
                
                if (_propertyValue)
                    [_instance setValue:_propertyValue forKey:_property];
                else
                {
                    BRLogInfo(@"Something went wrong when trying to parse the Object Property [%@] Mapped to JsonKey [%@] For Class [%@]",_property,_jsonKey,classType);
                }
            }
         
        }
        return _instance;
    }
    else if (theData != nil && ![theData isKindOfClass:[NSNull class]])
    {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"%s[Line %d]: Expected an NSDictionary [%@] found other %@ in [%@]",__PRETTY_FUNCTION__, __LINE__, self.classType, [theData class],theData ]
                                     userInfo:nil];
    }
    return nil;
}


@end
