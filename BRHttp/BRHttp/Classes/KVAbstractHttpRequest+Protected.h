//
//  KVAbstractHttpRequest+Protected.h
//  Rewardisement
//
//  Created by ELie Melki on 11/29/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import "KVAbstractHttpRequest.h"
#import "KVHttpCachableRequest.h"
#import "KVHttpPostObjectRequest.h"

/*
 * These function can be called/ovveride by subclasses
 * Function marked as To Override means that subclasses should ovveride them or they will crash, as of all these classes are considered abstract
 */

@interface KVAbstractHttpRequest (Protected)

- (void) startExecutingRequest;
- (void) executeSuccessRequest:(KVASIHttpRequest *)theRequest;

- (void) reportRequestDidFinish;
- (void) reportRequestFinishedWithError:(NSError *)theError;

- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL;

- (NSOperationQueue *) queue;

//To Override, If no parser Delegate specified this will have the Path of downloaded file.
- (void) doSomethingWithValue:(id)theValue;

- (NSError *) errorFromPostDownloadProcessing:(KVASIHttpRequest *)theRequest;
- (id) objectValueFromResponse:(KVASIHttpRequest *)theRequest;

@end


@interface KVHttpCachableRequest(Protected)

- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL cachePolicy:(ASICachePolicy)theCachePolicy;

@end

@interface KVHttpPostObjectRequest(Protected)

- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL withData:(NSData *)theData;


@end