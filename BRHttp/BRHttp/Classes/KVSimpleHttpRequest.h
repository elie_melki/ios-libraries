//
//  NSSimpleHttpRequest.h
//  NSFoundation
//
//  Created by ELie Melki on 3/17/13.
//  Copyright (c) 2013 ELie Melki. All rights reserved.
//

#import "KVAbstractHttpRequest.h"

@class KVSimpleHttpRequest;

@protocol KVSimpleHttpRequestDelegate <KVAbstractHttpRequestDelegate>

@optional
- (void) simpleHttpRequest:(KVSimpleHttpRequest *)theRequest didRecieveData:(NSData *)theData;

@end

@interface KVSimpleHttpRequest : KVAbstractHttpRequest

@property (nonatomic,assign) id<KVSimpleHttpRequestDelegate> delegate;

@end
