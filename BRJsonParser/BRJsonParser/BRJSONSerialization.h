//
//  KVJSONSerialization.h
//  Rewardisment
//
//  Created by ELie Melki on 10/22/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BRSerialization.h"

@interface BRJSONSerialization : NSObject

+ (id) deserializeWithData:(NSData *)theData options:(NSJSONReadingOptions)theOption error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable;
+ (id) deserialize:(id)object error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable;

+ (NSData *) serializeWithData:(id)theObject options:(NSJSONWritingOptions)theOption error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable;
+ (id) serialize:(id)theObject error:(NSError **)theError serialization:(id<BRSerialization>)theSerializable;

//Async
+ (void) deserializeWithData:(NSData *)theData options:(NSJSONReadingOptions)theOption serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError;
+ (void) deserialize:(id)object serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError;

+ (void) serializeWithData:(id)theObject options:(NSJSONWritingOptions)theOption serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(NSData *))onSuccess onError:(void (^)(NSError *))onError;
+ (void) serialize:(id)theObject serialization:(id<BRSerialization>)theSerializable onSuccess:(void (^)(id))onSuccess onError:(void (^)(NSError *))onError;



@end
