//
//  KVHttpCachableRequest.h
//  Rewardisement
//
//  Created by ELie Melki on 11/27/12.
//  Copyright (c) 2012 ELie Melki. All rights reserved.
//


#import "KVHttpCachableRequest.h"

#import "KVASIHTTPRequest.h"
#import "ASIDownloadCache.h"
#import "KVAbstractHttpRequest+Protected.h"
#import "Reachability.h"

@interface NSOperationQueue (ADRequest)
+ (NSOperationQueue *) cachedRequestQueue;
@end


@interface KVHttpCachableRequest()

@property (retain, nonatomic) NSString *responseMD5;
@property (nonatomic) BOOL firstRequest;



- (void) scheduleUpdateFromCache;
- (void) scheduleUpdateFromRemoteHost;
- (void) scheduleUpdate:(ASICachePolicy)theCachePolicy queue:(NSOperationQueue *)theQueue;
- (BOOL) wasRequestScheduledFromCache:(ASIHTTPRequest *)theRequest;

@end


@implementation KVHttpCachableRequest

@dynamic delegate;
@synthesize responseMD5, useMD5Comparisons,firstRequest;


//-------------------------------------
// Init & Dealloc
//-------------------------------------
#pragma mark - Init & Dealloc

- (id) initWithURL:(NSURL *)theURL parser:(id<KVASIHttpParseDelegate>)theParser
{
    self = [super initWithURL:theURL parser:theParser];
    
    if(self)
    {
        self.firstRequest = YES;
        needsRefresh = YES;
    }
    
    return self;
}

- (void) dealloc
{
    [responseMD5 release];
    [super dealloc];
}


//-------------------------------------
// Ovveride Methods
//-------------------------------------
#pragma mark - Ovveride Methods

- (void) startExecutingRequest
{
    if(firstRequest)
    {
        [self scheduleUpdateFromCache]; // Read the previous value from the cache if possible
    }
    else
    {
        [self scheduleUpdateFromRemoteHost];  // Check for an updated version remotely
    }
    firstRequest = NO;
}

- (void) cancel
{
    needsRefresh = NO;
    [super cancel];
}


- (void) executeSuccessRequest:(KVASIHttpRequest *)theRequest
{
    if([self wasRequestScheduledFromCache:theRequest])
    {
        [self didReceiveUpdatedResponse:theRequest];
        
        //Dont bother to check server update if there was no connection, so we dont overload the queue and resources
        if ([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable)
        {
            // Check the server for updated content - will call adapterRequestDidFinish: later
            [self scheduleUpdateFromRemoteHost];
        }
        else
        {
            [self reportRequestDidFinish];
        }
    }
    else
    {
        if(![theRequest didUseCachedResponseOrMD5Identical])
        {
            [self didReceiveUpdatedResponse:theRequest];
        }
        
        [self reportRequestDidFinish];
    }
}

//-------------------------------------
// Public interface
//-------------------------------------
#pragma mark - Public interface

- (void) setNeedsRefresh
{
    needsRefresh = YES;
}

//-------------------------------------
// Protected interface
//-------------------------------------
#pragma mark - Private interface


- (KVASIHttpRequest *) KVASIHttpRequestWithURL:(NSURL *)theURL cachePolicy:(ASICachePolicy)theCachePolicy
{
    KVASIHttpRequest *_request =  [super KVASIHttpRequestWithURL:theURL];
    _request.cachePolicy = theCachePolicy;
    _request.useMD5Comparisons = useMD5Comparisons;
    _request.previousResponseMD5 = self.responseMD5;
     return _request;
}


//-------------------------------------
// Private interface
//-------------------------------------
#pragma mark - Private interface

- (void) scheduleUpdateFromCache
{
    NSData *_data =  [self.cacheStorage cachedResponseDataForURL:self.URL];
    if (_data)
    {
        [self scheduleUpdate:ASIOnlyLoadIfNotCachedCachePolicy queue:[NSOperationQueue cachedRequestQueue]];
    }
    else
    {
        [self scheduleUpdate:ASIOnlyLoadIfNotCachedCachePolicy queue:self.queue];
    }
}

- (void) scheduleUpdateFromRemoteHost
{
    // Respects the max-age property of the cached content
    ASICachePolicy _policy = ASIUseDefaultCachePolicy;
    
    if(needsRefresh)
    {
        if(useMD5Comparisons)
        {
            _policy = ASIDoNotReadFromCacheCachePolicy;  // Forces a refresh so that we can do a client side modification check
        }
        else
        {
            _policy = ASIAskServerIfModifiedCachePolicy; // Server side modification check
        }
        
        needsRefresh = NO;
    }
    
    [self scheduleUpdate:_policy queue:self.queue];
}


- (void) scheduleUpdate:(ASICachePolicy)theCachePolicy queue:(NSOperationQueue *)theQueue
{
    KVASIHttpRequest *_request = [self KVASIHttpRequestWithURL:self.URL cachePolicy:theCachePolicy];
    
    [operation release];
    operation = [_request retain];
    [theQueue addOperation:operation];
}


- (BOOL) wasRequestScheduledFromCache:(ASIHTTPRequest *)theRequest
{
    return theRequest.cachePolicy == ASIOnlyLoadIfNotCachedCachePolicy && theRequest.didUseCachedResponse;
}

- (void) didReceiveUpdatedResponse:(KVASIHttpRequest *)theRequest
{
    self.responseMD5 = theRequest.responseMD5;
    
    id _value = [self objectValueFromResponse:theRequest];
    
    [self doSomethingWithValue:_value];
}

@end



//-------------------------------------
// NSOperationQueue Impl
//-------------------------------------
#pragma mark - NSOperationQueue Impl

@implementation NSOperationQueue (ADRequest)

+ (NSOperationQueue *) cachedRequestQueue
{
    static NSOperationQueue *queue;
    
    if(!queue)
    {
        queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount =  30;
    }
    
    return queue;
}


@end